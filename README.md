# bimodal-ssc

Bimodal Semantic Scene Completion

## Contents
0. [Organization](#organization)
0. [Requirements](#requirements)
0. [Compiling](#compiling)
0. [Data Path Setup](#data-path-setup)
0. [Datasets Preparation](#datasets-preparation)
0. [Extract Wavefront OBJ file from SunCG](#extract-wavefront-obj-file-from-suncg )
0. [Rendering Via Blender](#rendering-via-blender )

## Organization 
The project folder is organized as follows:
``` shell
    bimodal-ssc
         |-- blender               //Blender files for rendering
         |-- data                  //Auxiliary data  (cameras, ...)
         |-- scripts               //Usefull scripts (datasets dowload, etc)
         |-- python                //Python source code
         |-- src                   //cuda source code  
```

The data folders should be organized as follows:
``` shell
    suncg                     //SunCG RAW Dataset Root (see  http://sscnet.cs.princeton.edu/)
         |-- house                 //JSON scene files
         |-- object                //object models (Wavefront OBJ and MTL files) 
         |-- object_vox            //object occupancy
         |-- room                  //room structural objects (Floor, Ceiling, Walls)
         |-- texture               //object textures  

    nyu                      //NYU depth files and binary files with 3D ground truth
         |-- NYUtrain 
                        |-- xxxxx_0000.png
                        |-- xxxxx_0000.bin
         |-- NYUtest
                        |-- xxxxx_0000.png
                        |-- xxxxx_0000.bin
         |-- nyu_depth_v2_labeled.mat  //matlab data file containing NYU-D-V2 dataset 
```



## Requirements 
### Hardware Requirements (minimum):

   * Ubuntu 16.04 LTS or newer
   * For training: a 11G GPU (for example, a GTX 1080TI) in order to train EdgeNet with a batch-size of 3 scenes
   * For inference only: a 4G GPU (for example, a GTX 1050TI) 

### Software Requirements:

All software listed here are open-source.
 
   * GPU Drivers
   * Python 3
   * Anaconda 3: We recomend using an [Anaconda](https://www.anaconda.com/distribution/) virtual environment 
     for simplicity. 
   * Tensorflow with GPU/CUDA support. We suggest creating and activating a Anaconda virtual environment for 
     TensorFlow with GPU support using [these instructions](https://www.anaconda.com/tensorflow-in-anaconda/)
   * [Keras](https://anaconda.org/conda-forge/keras) .
   * [OpenCV 3 for python](https://anaconda.org/anaconda/py-opencv).
   * Python common libraries [pandas](https://anaconda.org/anaconda/pandas), 
                             [matplotlib](https://anaconda.org/conda-forge/matplotlib),
                             [sklearn](https://anaconda.org/anaconda/scikit-learn),
                             and other common packages according to your environment... 
For example:
``` shell
    conda install -c conda-forge keras
    conda install -c anaconda py-opencv
    conda install -c anaconda pandas
    conda install -c conda-forge matplotlib
    conda install -c anaconda scikit-learn
```
## Datasets preparation
Extract the provided cam.tar.gz file to a directory of your choice

Download and uncompress the SUNCG Dataset (see  http://sscnet.cs.princeton.edu/). 

Download and uncompress the NYU-D-V2 RAW Dataset (see  https://cs.nyu.edu/~silberman/datasets/nyu_depth_v2.html). 

Download the NYU-D-V2 3D labels and depth files from sscnet project(see https://github.com/shurans/sscnet) 

Copy the provided python/example_paths.conf to python/paths.conf and edit it according to your environment.

### Extracting NYU-D-V2 2D labels
``` shell
    python extract_nyu_2d_labels.py
```
