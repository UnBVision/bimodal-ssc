import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"
os.environ["KERAS_BACKEND"]="tensorflow"
from utils.file_utils import get_file_prefixes_from_path
from utils import path_config

import subprocess

import numpy as np # linear algebra
import multiprocessing
import argparse
import time
import struct
import shutil

import cv2
import math
import glob
import json



##################################
# Parameters
##################################
bin_path = {'test':'/home/adn/sscnet/data/depthbin/SUNCGtest_49700_49884/',
            'train':'/home/adn/sscnet/data/SUNCGtrain'}

TEMP_DIR = '/home/adn/output'
TRAIN_TEST = 'test'

path_dict = path_config.read_config()

suncgDataPath = path_dict["SUNCG_ROOT"]
suncgToolboxPath = path_dict["SUNCG_TOOLBOX"]
rgbdPath = path_dict["RGBD_ROOT"]
camPath = path_dict["CAM_ROOT"]
usemeasa = ''
pathtogaps = os.path.join(suncgToolboxPath, 'gaps/bin/x86_64')


def get_scene_nodes(sceneId):
    scene_path = os.path.join(suncgDataPath, "house", sceneId, "house.json")

    with open(scene_path) as json_file:
        scene = json.load(json_file)

    return scene

def get_cam_offset(scene: object, floorId: object, roomId: object) -> object:

    level_nodes = scene['levels'][floorId]['nodes']
    room = level_nodes[roomId]
    __, min_y, __ =  room['bbox']['min']
    return min_y + 0.05



def process_scene(sceneId, cam_list, bin_list):

    cam_base_path = os.path.join(camPath, TRAIN_TEST, sceneId[:2])
    os.makedirs(cam_base_path, exist_ok=True)

    cam_file = os.path.join(cam_base_path, sceneId + '.cam')
    f = open(cam_file, 'w')
    for cp in cam_list:
        f.write("%f %f %f %f %f %f %f %f %f %f %f %f \n" %
                (cp[0], cp[1], cp[2], cp[3], cp[4], cp[5],
                 cp[6], cp[7], cp[8], cp[9], cp[10], cp[11])
                )
    f.close()

    projectpath = os.path.join(suncgDataPath, 'house/' + sceneId)

    files_to_remove = glob.glob(os.path.join(TEMP_DIR,"*.jpg"), recursive=False)
    files_to_remove.extend(glob.glob(os.path.join(TEMP_DIR,"*.png"), recursive=False))
    for filePath in files_to_remove:
        try:
            os.remove(filePath)
        except OSError:
            print("Error while deleting file")
            return

    cmd_gen_rgb = format('unset LD_LIBRARY_PATH;\n'
                         ' cd  %s \n %s/scn2img house.json '
                         ' %s  -capture_depth_images  -capture_color_images -xfov 0.55 %s %s' %
                         (projectpath, pathtogaps, cam_file, TEMP_DIR, usemeasa))
    ret = os.system(cmd_gen_rgb)

    #ret  =subprocess.call(cmd_gen_rgb, shell=True)

    #ret  = 0 #not to generate images
    if ret != 0:
        print("Error:", ret, sceneId)
    else:
        caminfo_file = os.path.join(cam_base_path, sceneId + '.caminfo')
        f = open(caminfo_file, 'w')

        for n, bin_file in enumerate(bin_list):

            floorId = int(bin_file[-15:-12])-1
            roomId = int(bin_file[-9:-5])-1

            base_name=str(n).zfill(6)
            temp_color =  os.path.join(TEMP_DIR,base_name + "_color.jpg")
            temp_depth =  os.path.join(TEMP_DIR,base_name + "_depth.png")
            dest_path = os.path.join(rgbdPath, TRAIN_TEST, sceneId[:2])
            os.makedirs(dest_path, exist_ok=True)
            dest_color =os.path.join(dest_path, sceneId + bin_file[-18:-4]+base_name+"_color.jpg")
            dest_depth =os.path.join(dest_path, sceneId + bin_file[-18:-4]+base_name+"_depth.png")

            img_o = cv2.imread(temp_color)
            img_f = cv2.flip(img_o, 1)
            cv2.imwrite(dest_color, img_f)
            img_o = cv2.imread(temp_depth, cv2.IMREAD_ANYDEPTH)
            img_f = cv2.flip(img_o, 1)
            img_f = img_f.astype(np.float64)/1000
            img_f[np.isnan(img_f)] = 0
            img_f = img_f.astype(np.float32)*1000
            img_f = img_f.astype(np.uint16)
            img_f = np.left_shift(img_f,3) | np.right_shift(img_f,16-3)
            cv2.imwrite(dest_depth, img_f)

            f.write("Room#%d_%d_%d\n" % (floorId, roomId, 0))
            #print(bin_file,floorId,roomId)
        f.close()

file_prefixes_unsorted = get_file_prefixes_from_path(bin_path[TRAIN_TEST], criteria='*.bin')

scenes = [os.path.basename(x).split(sep="_")[1] for x in file_prefixes_unsorted]
idx = sorted(range(len(scenes)), key=scenes.__getitem__)

file_prefixes = [file_prefixes_unsorted[x] for x in idx]

print("Files to process:", len(file_prefixes))

sceneId_ant = ''
cam_list=[]
bin_list=[]
processed = 0
floorId = 99
roomId=99
scene_nodes = None

for file_prefix  in file_prefixes:

    sceneId = file_prefix[-50:-18]

    #if sceneId != "e6f4f4b45bb1badab15c2892a3ab9294":
    #    continue


    if processed % 15 == 0:
        print("Processed: %d (%.2f%%)        " % (processed, 100*processed/len(file_prefixes)), end="\r")

    if (sceneId != sceneId_ant):
        scene_nodes = get_scene_nodes(sceneId)

        if len(cam_list) >0:
            process_scene(sceneId_ant, cam_list, bin_list)

        sceneId_ant = sceneId
        cam_list=[]
        bin_list=[]
        qty = 0

    qty += 1
    processed += 1
    f = open(file_prefix + '.bin', 'rb')
    vox_origin = np.array(struct.unpack("3f", f.read(3 * 4)))
    ex = np.array(struct.unpack("16f", f.read(16 * 4)))
    f.close()

    floorId = int(file_prefix[-15:-12]) - 1
    roomId = int(file_prefix[-9:-5]) - 1

    y_offset = get_cam_offset(scene_nodes, floorId, roomId)

    #campose
    cam_list.append([ex[3], ex[11] + y_offset, ex[7], ex[2], ex[10], ex[6], -ex[1], -ex[9], -ex[5], 0.55, 0.388863, 17.0])
    bin_list.append(file_prefix)


process_scene(sceneId_ant, cam_list, bin_list)
print("Processed: %d (%.2f%%)        " % (processed, 100 * processed / len(file_prefixes)))
